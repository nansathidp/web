import Vue from 'vue'
import Vuex from 'vuex'
import VueMaterial from "vue-material";
import VueLogger from 'vuejs-logger'
import AsyncMethods from 'vue-async-methods'


const options = {
    logLevel : 'debug',
    // optional : defaults to false if not specified
    stringifyArguments : false,
    // optional : defaults to false if not specified
    showLogLevel : false
};

Vue.use(VueLogger, options);
Vue.use(AsyncMethods);
Vue.use(Vuex);
Vue.use(VueMaterial);

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  }
})
