FROM node:10-jessie 
RUN apt-get -y update && \
   apt-get install -y build-essential python libx11-dev libxtst-dev curl \
   && npm install -g node-gyp
RUN mkdir /app
COPY package.json /app/
WORKDIR /app
RUN npm install
RUN npm install -g @vue/cli

ADD . /app/
RUN ln -fs /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && dpkg-reconfigure -f noninteractive tzdata
EXPOSE 8080
CMD npm run serve
